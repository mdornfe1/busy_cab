import psycopg2
import pandas.io.sql as sqlio
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import folium
import json
from IPython import embed
from passwords import postgres_password

plt.style.use('bmh')
 
MAP_CONN = psycopg2.connect(host='localhost', dbname='nyc_routing', user='postgres', password=postgres_password)

day_names = ['sun', 'mon', 'tues', 'wed', 'thurs', 'fri', 'sat']

"""
def calc_shortest_path(source, target, day, hour):
	sql = ("DROP TABLE IF EXISTS d_path; DROP TABLE IF EXISTS d_path2;" +
		"CREATE TABLE d_path AS SELECT seq, id1 AS node, id2 AS edge, cost FROM pgr_dijkstra('SELECT gid as id, source::int4, target::int4, jan_2015_cost::float8 AS cost, jan_2015_reverse_cost::float8 AS reverse_cost FROM ways_costs', %(source)s, %(target)s, True, True);" +
	"CREATE TABLE d_path2 AS SELECT node, lon, lat, edge FROM ways_vertices_pgr, d_path where ways_vertices_pgr.id = d_path.node;" + 
	"SELECT node, lon, lat, edge, ST_AsGeoJSON(the_geom) as geojson FROM d_path2, ways_costs WHERE ways_costs.gid=d_path2.edge;")
	
	df = sqlio.read_sql(sql % {'source':source, 'target': target}, MAP_CONN)

	df.geojson = [json.loads(gj.replace("'", "\"")) for gj in df.geojson]
	df.geojson = [gj for gj in df.geojson]

	return df
"""

def calc_shortest_path(source, target, day, hour, weight):
	"""Input source node and target node ids. In the table ways_vertices_pgr
	this is labeled as id not osm_id. Returns the k shortest paths between these
	two nodes.
	"""
	sql = ("DROP TABLE IF EXISTS d_path; DROP TABLE IF EXISTS d_path2;" +
		"CREATE TABLE d_path AS SELECT seq, id1 AS node, id2 AS edge, cost FROM pgr_dijkstra('SELECT gid as id, source::int4, target::int4, %(weight)s * %(day)s_%(hour)s_cost::float8 AS cost, %(weight)s * %(day)s_%(hour)s_reverse_cost::float8 AS reverse_cost FROM way_costs2', %(source)s, %(target)s, True, True);" +
	"CREATE TABLE d_path2 AS SELECT node, lon, lat, edge FROM ways_vertices_pgr, d_path where ways_vertices_pgr.id = d_path.node;" + 
	"SELECT node, lon, lat, edge, ST_AsGeoJSON(the_geom) as geojson FROM d_path2, way_costs2 WHERE way_costs2.gid=d_path2.edge;")
	
	df = sqlio.read_sql(sql % {'source':source, 'target': target, 'day':day_names[day], 'hour':hour, 'weight':1/weight}, MAP_CONN)

	df.geojson = [json.loads(gj.replace("'", "\"")) for gj in df.geojson]
	df.geojson = [gj for gj in df.geojson]

	return df

def get_nearest_node(lon, lat):
	"""Input lon, lat returns nearest osm_node"""
	sql = "SELECT * FROM ways_vertices_pgr ORDER BY the_geom <-> ST_GeometryFromText('POINT(%(lon)s %(lat)s )',4326) LIMIT 1;"

	df = sqlio.read_sql(sql % {'lon':lon, 'lat': lat}, MAP_CONN)

	return df['id'][0], df

def get_way(gid):
	sql = "SELECT * FROM ways WHERE gid=%(gid)s;"
	df = sqlio.read_sql(sql % {'gid':gid}, MAP_CONN)

	return df

def get_nearest_way(lon, lat):
	"""Input lon, lat returns nearest osm_way"""
	sql = "SELECT * FROM ways ORDER BY the_geom <-> ST_GeometryFromText('POINT(%(lon)s %(lat)s )',4326) LIMIT 1;"

	df = sqlio.read_sql(sql % {'lon':lon, 'lat': lat}, MAP_CONN)

	return df['gid'][0], df

def plot_paths(paths_df, source_df, target_df, fig=None, ax=None):
	if ax is None:
		fig, ax = plt.subplots()

	for route in np.unique(paths_df['route']):
		idx = paths_df['route'] == route
		lats = paths_df[idx]['lat']
		lons = paths_df[idx]['lon']
		ax.plot(lons, lats, label='path' + str(route))

	ax.plot(source_df['lon'], source_df['lat'], 'o', label='source', color='green')
	ax.plot(target_df['lon'], target_df['lat'], 'o', label='target', color='red')
	ax.set_xlabel('Longitude (degrees)')
	ax.set_ylabel('Latitude (degrees)')		
	#ax.legend()

	return fig, ax 

def get_cost(gid=None):
	"""
	Inputs gid of way. Returns cost and reverse_cost of that way.
	If no gid is specified returns all rows.
	"""
	if gid is None:
		sql = "SELECT gid, one_way, cost, reverse_cost FROM ways;"
	
		return sqlio.read_sql(sql, MAP_CONN)

	else:
		sql = "SELECT gid, one_way, cost, reverse_cost FROM ways WHERE gid=%(gid)s;" 
		
		return sqlio.read_sql(sql % {'gid':gid}, MAP_CONN)

if __name__ == '__main__':
	lon_insight = -73.9861860
	lat_insight = 40.7419650
	lon_home = -73.9576270
	lat_home = 40.7607880
	lon_world_trade = -74.0131
	lat_world_trade = 40.7130
	lon_bedford = -73.957504
	lat_bedford = 40.717883



	insight_id, df_insight = get_nearest_node(lon_world_trade, lat_world_trade)
	home_id, df_home = get_nearest_node(lon_bedford, lat_bedford)
	df_path = calc_shortest_path(insight_id, home_id, day=1, hour=15, weight=1)
	
	nyc_map = folium.Map(location=[lat_insight, lon_insight], zoom_start=10)
	[folium.PolyLine(np.array(gj['coordinates'])[:,[1,0]]).add_to(nyc_map) for gj in df_path.geojson]
	nyc_map.save('route.html')