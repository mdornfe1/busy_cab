import sqlalchemy as sa
import psycopg2
import pandas.io.sql as sqlio
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import folium
import json
import requests
from IPython import embed
import datetime
from folium import plugins
from passwords import postgres_password
from scipy import spatial

plt.style.use('bmh')

MAP_CONN = psycopg2.connect(host='localhost', dbname='nyc_routing', user='postgres', password=postgres_password)

def get_nearest_node_id(lon, lat):
	"""Input lon, lat returns nearest osm_node"""
	sql = "SELECT * FROM ways_vertices_pgr ORDER BY the_geom <-> ST_GeometryFromText('POINT(%(lon)s %(lat)s )',4326) LIMIT 1;"

	df = sqlio.read_sql(sql % {'lon':lon, 'lat': lat}, MAP_CONN)

	return df['id'][0]

def get_nearest_way_id(lon, lat):
	"""Input lon, lat returns nearest osm_way"""
	sql = "SELECT * FROM ways ORDER BY the_geom <-> ST_GeometryFromText('POINT(%(lon)s %(lat)s )',4326) LIMIT 1;"

	df = sqlio.read_sql(sql % {'lon':lon, 'lat': lat}, MAP_CONN)

	return df['gid'][0]

def get_nearest_way_id(ways_matrix, ways_tree, lon, lat):
	nlonlat = ways_matrix[ways_tree.query([lon,lat])[1]] # <-- the nearest point 

sql = "SELECT pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude FROM yellow_trip_data;"
df = sqlio.read_sql(sql, MAP_CONN)

sql = "SELECT gid, x1, y1, x2, y2 FROM ways;"
df_ways = sqlio.read_sql(sql, MAP_CONN, index_col='gid')
ways_matrix = np.vstack(((df_ways['x2'] - df_ways['x1'])/2 + df_ways['x1'],
	(df_ways['y2'] - df_ways['y1'])/2 + df_ways['y1'] )).T 
ways_tree = spatial.KDTree(ways_matrix)

sql = "SELECT id, lon, lat FROM ways_vertices_pgr;"
df_nodes = sqlio.read_sql(sql, MAP_CONN, index_col='id')
nodes_matrix = df_nodes.as_matrix()
nodes_tree = spatial.KDTree(nodes_matrix)

df_ways_nodes = pd.DataFrame(index=df.index, columns=['pickup_way_id', 'dropoff_way_id', 'pickup_node_id', 'dropoff_node_id'])
t0 = time.time()
for i, row in enumerate(df.iterrows()):
	if i==1000:
		break
	print(str(i) + '/' + str(len(df)))
	trip_id = row[0]
	pickup_longitude = row[1]['pickup_longitude']
	pickup_latitude = row[1]['pickup_latitude']
	dropoff_longitude = row[1]['dropoff_longitude']
	dropoff_latitude = row[1]['dropoff_latitude']

	df_ways_nodes['pickup_way_id'][trip_id] = get_nearest_way_id(pickup_longitude, pickup_latitude)
	df_ways_nodes['pickup_node_id'][trip_id] = get_nearest_node_id(pickup_longitude, pickup_latitude)
	df_ways_nodes['dropoff_way_id'][trip_id] = get_nearest_way_id(dropoff_longitude, dropoff_latitude)
	df_ways_nodes['dropoff_node_id'][trip_id] = get_nearest_node_id(dropoff_longitude, dropoff_latitude)


t1 = time.time()