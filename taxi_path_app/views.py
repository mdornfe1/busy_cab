from taxi_path_app import app
from flask import render_template, request
import sqlalchemy as sa
from sqlalchemy_utils import database_exists, create_database
import pandas as pd
import pandas.io.sql as sqlio
import calculate_path as cp
import datetime
import os
from passwords import postgres_password

user = 'postgres'
host = 'localhost'
dbname = 'nyc_taxi_data'
TAXI_CONN = sa.create_engine('postgresql://postgres:' + postgres_password + '#@localhost:5432/nyc_taxi_data')


@app.route('/')
@app.route('/index')
def pickups_input():
	return render_template("map.html")

@app.route('/output')
def pickups_output():
 
	#day = int(request.args.get('day'))
	#hour = int(request.args.get('hour'))
	#start_address = request.args.get('start_address')
	#end_address = request.args.get('end_address')
	source_lon = float(request.args.get('source_lon'))
	source_lat = float(request.args.get('source_lat'))
	weight = 10
	day = datetime.datetime.today().weekday()
	hour = datetime.datetime.today().hour
	print(type(hour))
	if hour == 23:
		hour = 22

	#source_lon, source_lat = cp.get_gps_from_address(start_address)
	
	route_template = cp.main(source_lon, source_lat, day, hour, weight)
	route_template = 'tmp/' + os.path.split(route_template)[1]

	return render_template(route_template)
